#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
/*
 *  RIP - Information retrieval data porta, used as a search engine to search
 *  over the course compiled document collection
 *
 *  Copyright (C) 2011
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.ucr.pf3394.tarea9.utils.results;

import java.util.Comparator;

/**
 *
 * @author asanabria
 */
public class ResultQueryRecordCompartor implements Comparator<ResultQueryRecord> {

    public int compare(ResultQueryRecord rqr1, ResultQueryRecord rqr2) {
        Double d1 = rqr1.ranking;
        Double d2 = rqr2.ranking;
        return d2.compareTo(d1);
    }
}