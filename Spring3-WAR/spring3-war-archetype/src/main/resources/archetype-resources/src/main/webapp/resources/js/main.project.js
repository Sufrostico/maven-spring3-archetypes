
$(document).ready(function(){

    // focus on the text field
    $("#searchInput").focus();                                                                                                             
 
    //Click on search link start the search
    $("#simple").click(function(){                                                                                                         

        var searchString = $('#searchInput').val();                                                                                        
        $('#resultsPanel').html('');
        window.location.hash = searchString;
        search(searchString)
    }).mouseover(function(){
        $(this).css('color', '#C03818');
    }).mouseout(function(){
        $(this).css('color', '#FA6900');
    });


    //Enter start the search.
    $("#searchInput").keypress(function (e) {
        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
            $('#simple').click();
            return false;
        } else {
            return true;
        }
    });
    
    checkHash();
});

function search( searchString ){
    $('#resultsPanel').xslt(
    contextPath+'api/search?q='+searchString, 
    contextPath+"resources/xsl/results.xsl", 
    colors);
}

function colors(){
    $('#results div').each(function(){
        
        $(this).mouseover(function(){
            $(this).css('background-color', 'black');
        }).mouseout(function(){
            $(this).css('background-color', 'white');
        })
    });
}

function checkHash(){
    
    var searchString = window.location.hash.replace('#', '');
    search(searchString);
    $('#searchInput').val(searchString);
}