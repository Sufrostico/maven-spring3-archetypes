#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
/*
 *  RIP - Information retrieval data porta, used as a search engine to search
 *  over the course compiled document collection
 *
 *  Copyright (C) 2011
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.ucr.pf3394.tarea9.web.controller;

import edu.ucr.pf3394.tarea9.domain.PostingRecord;
import edu.ucr.pf3394.tarea9.domain.QueryTerm;
import edu.ucr.pf3394.tarea9.utils.ri.RIUtils;
import edu.ucr.pf3394.tarea9.domain.URLRecord;
import edu.ucr.pf3394.tarea9.domain.VocabularyRecord;
import edu.ucr.pf3394.tarea9.utils.results.ResultQueryRecord;
import edu.ucr.pf3394.tarea9.utils.results.ResultQueryRecordCompartor;
import edu.ucr.pf3394.tarea9.utils.results.XMLResults;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author asanabria
 */
@Controller
@RequestMapping("/api/*")
public class MainController {

    @Autowired
    RIUtils riutils;

    /**
     * Get a well formated xml containing paginated species
     * @param searchString
     * @return
     */
    @RequestMapping(value = "/search*",
    method = RequestMethod.GET)
    public @ResponseBody
    XMLResults searchTaxonDescriptionWriteXml(
        @RequestParam
            (value = "q", defaultValue = "Aurelio", required = false)
            String query,
        
        @RequestParam
            (value = "startIndex", defaultValue = "0", required = false)
            int startIndex,
        
        @RequestParam
            (value = "results", defaultValue = "20", required = false) 
            int results) {

        Map<String, VocabularyRecord> vocabulary = riutils.getVocabulary();

        // Clean the query
        Map<String, QueryTerm> queryTerms = riutils.cleanSearchTerm(query);

        List<PostingRecord> postings;
        Set<Long> doctos = new TreeSet<Long>();

        //get the postings
        for (String queryTerm : queryTerms.keySet()) {
            postings = riutils.getDocumentsByTerm(queryTerm);

            if (postings.isEmpty()) {
                queryTerms.remove(query);
            }

            for (PostingRecord posting : postings) {
                doctos.add(posting.getDoctoID());
                queryTerms.get(queryTerm).addDocto( posting.getDoctoID(), 
                                                    posting.getWeigth());
            }
        }


        // Calculate the query weights.

        QueryTerm queryTerm = null;

        for (String term : queryTerms.keySet()) {
            queryTerm = queryTerms.get(term);
            riutils.calculateQueryWeight(queryTerm, vocabulary.get(term));
        }


        // Calculate the query norm
        double queryNorm = riutils.calculateQueryNorm(queryTerms);

        // Get all the related documents.
        XMLResults queryResult = new XMLResults();
        ResultQueryRecord rqr = null;
        URLRecord urlRecord = null;

        // Calculate numerator
        for (Long docId : doctos) {
            double numerator = 0D;
            for (String term : queryTerms.keySet()) {
                queryTerm = queryTerms.get(term);
                numerator += queryTerm.getWeight() 
                                * queryTerm.getWeightOnDocto(docId);
            }

            // Calculate denominator
            urlRecord = riutils.getURLDocument(docId);

            double doctoNorm = urlRecord.getNorm();
            double denominator = queryNorm * doctoNorm;
            double ranking = numerator / denominator;

            String url = urlRecord.getUrl().trim().replace("${symbol_escape}0", " ");
            String[] parts = url.split(" ");
            if (parts.length > 1) {
                url = parts[1];
            }

            rqr = new ResultQueryRecord(docId + "", url, ranking);
            queryResult.addResultQueryRecord(rqr);

        }
        Collections.sort(queryResult.queryResults,
                            new ResultQueryRecordCompartor());

        queryResult.quantity = queryResult.queryResults.size();
        
        if (queryResult.queryResults.size() > results) {
            queryResult.queryResults = 
                queryResult.queryResults.subList(startIndex, results);
        }

        return queryResult;
    }
}
