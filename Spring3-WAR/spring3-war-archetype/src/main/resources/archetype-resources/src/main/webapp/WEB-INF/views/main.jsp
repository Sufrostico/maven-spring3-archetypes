#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><fmt:message key="title"/></title>

        <!-- css -->
        <link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/main.project.css' />" />

        <!-- javascript -->
        <script type="text/javascript" src="<c:url value='/resources/js/main.project.js' />"></script>
        <script type="text/javascript" src="<c:url value='/resources/js/xslt.project.js' />"></script>

        <!-- Main JQuery -->
        <script type="text/javascript" src="<c:url value='/resources/js/jquery-1.6.1-min.js' />"></script>
        <script type="text/javascript" src="<c:url value='/resources/js/jqueryui-1.8.13-custom.min.js' />"></script>

        <!-- Usefull Global Variables -->
        <script type="text/javascript">
            var contextPath = '<c:out value="${symbol_dollar}{pageContext.request.contextPath}" />/';
            </script>
        </head>

        <body>
            <div>
                <!-- Header -->
                <div id="header"></div>
                <!-- Content -->
                <div id="content">
                    <h1>Spring3 WAR project</h1>
                </div> 
                <!-- Footer -->
                <div id="footer"></div>
            </div>
        </body>    
    </html>
