/*
 *  RIP - Information retrieval data porta, used as a search engine to search
 *  over the course compiled document collection
 *
 *  Copyright (C) 2011
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.ucr.pf3394.tarea9.utils.results;

import javax.xml.bind.annotation.XmlElement;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;

/**
 * @author asanabria
 */
@RooJavaBean
@RooToString
public class ResultQueryRecord {

    @XmlElement(name = "name")
    public String name;
    @XmlElement(name = "url")
    public String url;
    @XmlElement(name = "ranking")
    public double ranking;

    /**
     * No argument contructor
     */
    public ResultQueryRecord() {
        super();
    }

    public ResultQueryRecord(String name) {
        this.name = name;
    }

    public ResultQueryRecord(String name, String url, double ranking) {
        this.name = name;
        this.url = url;
        this.ranking = ranking;
    }
}
