<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <xsl:template match="response">
        <div style="text-align: right">
            Mostrando 20 de <xsl:value-of select='documentCount' /> resultados.
        </div>
        <xsl:apply-templates select="documents" />
    </xsl:template>

    <xsl:template match="documents">
        <hr />
        <div id="results">
            <xsl:for-each select="document">
                <div>
                    <a target="_blank">
                        <xsl:attribute name="href">
                            <xsl:value-of select="url" />
                        </xsl:attribute> 
                        <xsl:value-of select="url" />
                    </a>
                    <br />
                    <p>
                        <table>
                            <tr>
                                <td>Document:</td>
                                <td>
                                    <xsl:value-of select="name" />
                                    <xsl:text>&#160;&#160;&#160;&#160;</xsl:text>
                                    <a target="_blank">
                                        <xsl:attribute name="href">
                                            http://ara.inbio.ac.cr/collection.cache/<xsl:value-of select="name" />
                                        </xsl:attribute>(en caché)</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Ranking:</td>
                                <td><xsl:value-of select="ranking" /></td>
                            </tr>
                        </table>
                    </p>
                </div>
            </xsl:for-each>
        </div>
    </xsl:template>
</xsl:stylesheet>